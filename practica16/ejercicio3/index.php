<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */

spl_autoload_register(function ($nombre_clase) {
    include "./clases/" . $nombre_clase . '.php';
});

$planeta1 = new Planetas("pluton", 5, 6.9E+22, 0.7E+10, 2370, 59100000000, "ENANO");

$planeta2 = new Planetas();

echo $planeta1->valoresatributos();

echo "<br>";

echo $planeta1->densidad();

echo "<br>";

echo $planeta1->exterior();

echo "<br>";

echo $planeta2->valoresatributos();

echo "<br>";

echo $planeta2->densidad();

echo "<br>";

echo $planeta2->exterior();

echo "<br>";

var_dump($planeta1, $planeta2);
