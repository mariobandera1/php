<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Planetas
 *
 * @author Mario
 */
class Planetas {

    public ?string $nombre;
    public int $cantidad_de_satelites = 0;
    public float $masa_en_kilogramos = 0.0;
    public float $volum_cubic = 0.0;
    public int $diametro = 0;
    public int $dist_media = 0;
    public string $tipo_planet;
    public bool $observable = false;
    
    const UA=149597870;

    //put your code here
    public function __construct(string $nombre = null, int $cantidad_de_satelites = 0, float $masa_en_kilogramos = 0, float $volum_cubic = 0, int $diametro = 0, int $dist_media = 0, string $tipo_planet = "", bool $observable = false) {
        $this->nombre = $nombre;
        $this->cantidad_de_satelites = $cantidad_de_satelites;
        $this->masa_en_kilogramos = $masa_en_kilogramos;
        $this->volum_cubic = $volum_cubic;
        $this->diametro = $diametro;
        $this->dist_media = $dist_media;
        $this->tipo_planet = $tipo_planet;
        $this->observable = $observable;
    }

    public function valoresatributos() {

        $salida = "nombre={$this->nombre}";

        $salida .= "<br>";

        $salida = $salida . "cantidad_de_satelites={$this->cantidad_de_satelites}";

        $salida = $salida . "<br>";

        $salida .= "masa_en_kilogramos= {$this->masa_en_kilogramos}";

        $salida = $salida . "<br>";

        $salida .= "volum_cubic ={$this->volum_cubic}";

        $salida = $salida . "<br>";

        $salida .= "diametro={$this->diametro}";
        
          $salida = $salida . "<br>";

        $salida .= "dist_media= {$this->dist_media}";

        $salida = $salida . "<br>";

        $salida .= "tipo_planet ={$this->tipo_planet}";

        $salida = $salida . "<br>";

        $salida .= "obserbable={$this->observable}";

        return $salida;
    }

    public function densidad(): float {
        $masa = $this->masa_en_kilogramos;
        $volumen = $this->volum_cubic;
        $salida = $masa / $volumen;

        echo "la densidad es ";
        return $salida;
    }

    public function exterior(): bool {

                
      

        if ($this->dist_media > (3.4 *self::UA)) {
            
            echo "el planeta es exterior";
            
            echo "<br>";

            return true;

            
        } else {
            
            echo "<br>";
            
            echo "el planeta es interior";
            return false;

            
        }
    }

}
