<?php

spl_autoload_register(function ($nombre_clase) {
    include "./clases/" . $nombre_clase . '.php';
});

$dimension = new Dimension(5, 10, 40);

echo $dimension->getVolumen();

var_dump($dimension);

echo $dimension;