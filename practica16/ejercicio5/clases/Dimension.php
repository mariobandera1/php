<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Dimension
 *
 * @author Mario
 */
class Dimension {
    
    public float $alto;
    
    public float $ancho;
    
    public float $profundidad;
    
    public function __construct(float $alto = 0, float $ancho=0, float $profundidad=0) {
        $this->alto = $alto;
        $this->ancho = $ancho;
        $this->profundidad = $profundidad;
    }
    public function getAlto(): float {
        return $this->alto;
    }

    public function getAncho(): float {
        return $this->ancho;
    }

    public function getProfundidad(): float {
        return $this->profundidad;
    }

    public function setAlto(float $alto): void {
        $this->alto = $alto;
    }

    public function setAncho(float $ancho): void {
        $this->ancho = $ancho;
    }

    public function setProfundidad(float $profundidad): void {
        $this->profundidad = $profundidad;
    }

    public function getVolumen(){
       $resultado= $this->alto*$this->ancho*$this->profundidad;
       
       return $resultado;
       
    
//put your code here
    }
    public function __toString() {
        
        $resultado1= $this->alto.",".$this->ancho.",".$this->profundidad;
        
        return $resultado1;
    }
}
