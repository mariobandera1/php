<?php


/**
 * Description of Superheroe
 *
 * @author Mario
 */
class Superheroe {
    //put your code here
    
    public string $nombre;
    
    public string $descripcion="";
    
    public bool $capa=false;
    
    public function __construct(string $nombre) {
        $this->nombre = $nombre;
    }
    public function getNombre(): string {
        return $this->nombre;
    }

    public function getDescripcion(): string {
        return $this->descripcion;
    }

    public function getCapa(): string { 
        
        if($this->capa){
            
            return "tiene capa";
            
        } else {
            
            return "no tiene capa";
            
        }
            
         

        //puesto en string, auqnue por defecto era bool
          
    }

    public function setNombre(string $nombre): void {
        $this->nombre = $nombre;
    }

    public function setDescripcion(string $descripcion): void {
        $this->descripcion = $descripcion;
    }

    public function setCapa(bool $capa): void {
        $this->capa = $capa;
    }
    
    public function __toString() {
        $resultado=$this->nombre.",".$this->descripcion.",".$this->getCapa();
        
        return $resultado;
    }

}
