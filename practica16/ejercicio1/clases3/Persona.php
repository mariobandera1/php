<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Persona
 *
 * @author Mario
 */
class Persona {
    
    public string $nombre="Pedro";
    public string $apellido="Pérez";
    public string $numero_dni="105312010";
    public int $ano_nacimiento=1998;
    
  /** @deprecated  este metodo es valido pero ya no se usa, es depreciado.
   public function imprimePantalla(){
       
       echo "nombre={$this->nombre}";
       
       echo"<br>";
       
       echo  "apellido={$this->apellido}";
       
       echo"<br>";
       
      echo  "numero DNI= {$this->numero_dni}";
      
       echo"<br>";
       
      echo  "año de nacimiento {$this->ano_nacimiento}";
       
        
   }  */      
    
    //en la nueva se guarda el valor en la variable y se van concatenando los siguientes con el ., 
    public function imprimeNuevo() : string {
        
        $salida= "nombre={$this->nombre}";
       
       $salida.= "<br>";
       
      $salida= $salida. "apellido={$this->apellido}";
       
       $salida=$salida."<br>";
       
      $salida.= "numero DNI= {$this->numero_dni}";
      
       $salida=$salida."<br>";
       
       return $salida;
        
        
    }
    
    
   public function __construct(string $nombre, string $apellido, string $numero_dni, int $ano_nacimiento) {
       $this->nombre = $nombre;
       $this->apellido = $apellido;
       $this->numero_dni = $numero_dni;
       $this->año_nacimiento = $ano_nacimiento;
   }
   
   public function __toString() {
       
       $salida= "nombre={$this->nombre}";
       
       $salida.= "<br>";
       
      $salida= $salida. "apellido={$this->apellido}";
       
       $salida=$salida."<br>";
       
      $salida.= "numero DNI= {$this->numero_dni}";
      
       $salida=$salida."<br>";
   }

    //put your code here
}
