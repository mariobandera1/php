<!DOCTYPE html>
<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHPWebPage.php to edit this template
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // el calclo del descuento depende de la cantidad
        //cantidad<1000 ==>descuento1%
        //cantidad >=1000 y <10000 ==> descuento 2%
        //cantidad >=10000 ==> descuento 5%

        $cantidad = 1000;

        if ($cantidad < 1000) {
            $descuento = 0.001;
        } elseif ($cantidad < 10000) {
            $descuento = 2 / 100;
        } else {
            $descuento = 5 / 100;
        }
        echo $descuento;

        //metodo alternativo con switch
        //se pone con switch pero ya sepone el true, para hacer las preguntas en lugar de tener un numero asignado
        switch (true) {
            case ($cantidad < 1000):
                $descuento = 1 / 100;

                break;
            case ($cantidad < 10000):
                $descuento = 2 / 100;

                break;

            default:
                $descuento = 5 / 100;
                break;
        }
        echo $descuento;
        ?>

    </body>
</html>
