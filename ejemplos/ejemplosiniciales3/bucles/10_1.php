<!DOCTYPE html>
<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHPWebPage.php to edit this template
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $elementos = [
            ["color" => "red",
                "ancho" => "300 px",
                "alto" => "20px",
                "texto" => "ejemplo 1",
            ],
            ["color" => "blue",
                "ancho" => "500 px",
                "alto" => "50px",
                "texto" => "ejemplo 2"],
            ["color" => "green",
                "ancho" => "200px",
                "alto" => "30px",
                "texto" => "ejemplo 3"],
                ];
        // con for
                
        for ($index = 0; $index < count($elementos); $index++) {
    
                    $elemento=$elementos[$index];
            
        
         ?>       
        <div style="background-color:<?=($elemento)["color"]?> ;width:<?=($elemento)["ancho"]?>  ;height: <?=($elemento)["alto"]?>">
        
        <h5><?=($elemento["texto"]) ?></h5>
        
        </div>
        <?php
        }
         ?>
    </body>
</html>
