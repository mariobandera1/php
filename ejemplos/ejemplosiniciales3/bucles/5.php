<!DOCTYPE html>
<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHPWebPage.php to edit this template
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        //enlaces es un array enumerado, y en cada posicion del mismo hay un array asociativo
        
        $enlaces=[
            [
                "label"=>"buscador google",
                "link"=>"http://www.google.es",
                
            ],
            [
                "label"=>"buscador bing",
                "link"=>"https://www.bing.com",
            ],
            [
                "label"=> "pagina web de alpe",
                "link"=>"https://alpeformacion.es",
            ]
        ];
        
        
        
        // comienzo el menu
        foreach ($enlaces as $indice=>$enlace){
            //al ponerlo asi, el indice que estaba antes se guarda en $enlace se pasa con el bucle por cada indice
            //cada elemento de enlace tiene dos elemento, "link" y "enlace"
            
        ?>
        <div>
        
        <a href="<?=$enlace["link"]?>"> <?=$enlace["label"]?> </a>
        </div>
        
        
        <?php
        
        //fin del menu
        }
        ?>
        
        
    </body>
</html>
