<!DOCTYPE html>
<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHPWebPage.php to edit this template
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        //enlaces es un array enumerado, y en cada posicion del mismo hay un array asociativo
        //este lo haremos con while
        
        $enlaces=[
            [
                "label"=>"buscador google",
                "link"=>"http://www.google.es",
                
            ],
            [
                "label"=>"buscador bing",
                "link"=>"https://www.bing.com",
            ],
            [
                "label"=> "pagina web de alpe",
                "link"=>"https://alpeformacion.es",
            ]
        ];
        
        
        
        // comienzo el menu
        
        $c=0;
        
        while ($c<count($enlaces)) {
    

    

            //al ponerlo asi, el indice que estaba antes se guarda en $enlace se pasa con el bucle por cada indice
            //cada elemento de enlace tiene dos elemento, "link" y "enlace"
            
        ?>
        <div>
        
        <a href="<?=$enlaces[$c]["link"]?>"> 
                <?=$enlaces[$c]["label"]?> </a>
        </div>
        
        
        <?php
        $c++;
        //fin del menu
        }
        ?>
        
        
    </body>
</html>
