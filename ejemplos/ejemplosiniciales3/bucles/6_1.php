<!DOCTYPE html>
<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHPWebPage.php to edit this template
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        //enlaces es un array enumerado, y en cada posicion del mismo hay un array asociativo
        //concretamente en este hay un array grande, y dentro un array con tres posiciones que cada una tiene dos eementos
        //por eso lo que era $enlaces en el ejercicio 5, ahora es $enlaces[1]
        //en este en lugar de foreach lo hacemos con un for

        $enlaces = [
            [
                "title" => "enlaces principales",
            ],
            [
                [
                    "label" => "buscador google",
                    "link" => "http://www.google.es",
                ],
                [
                    "label" => "buscador bing",
                    "link" => "https://www.bing.com",
                ],
                [
                    "label" => "pagina web de alpe",
                    "link" => "https://alpeformacion.es",
                ]
            ]
        ];
        //colocamos el titulo del menu
        //
        ?>
        
        <?php
        // comienzo el menu
        
        for ($c = 0; $c < count($enlaces[1]); $c++) {
    

            ?>
            <div>

                <a href="<?= $enlaces[1][$c]["link"] ?>"> <?= $enlaces[1][$c]["label"] ?> </a>
            </div>


            <?php
            //fin del menu
        }
        ?>


    </body>
</html>
