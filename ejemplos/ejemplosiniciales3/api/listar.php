<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // quiero todas las peliculas
        $url="http://localhost/mario/apiRest/ejemplo1restyii/web/index.php/pelicula";
        $salida=file_get_contents($url);
        $salida= json_decode($salida);
        var_dump($salida); // todas
        var_dump($salida[0]); // la primera pelicula
        
        // quiero 100 numeros
        $url="http://localhost/mario/apiRest/ejemplo1restyii/web/index.php/site/numeros?cantidad=100";
        $salida=file_get_contents($url);
        $salida= json_decode($salida);
        var_dump($salida); // todas
        
        
        // quiero 12 letras
        $url="http://localhost/mario/apiRest/ejemplo1restyii/web/index.php/site/letras?cantidad=12";
        $salida=file_get_contents($url);
        $salida= json_decode($salida);
        var_dump($salida); // todas
        
        
        ?>
    </body>
</html>
