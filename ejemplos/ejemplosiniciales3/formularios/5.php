<?php
//cuando se de al boton de enviar, que calcule la suma, el producto y la media de los 3 numeros,
// suma con array_sum 
// producto con array_product,
// la media utilizando una funcion propia
?>
<!DOCTYPE html>
<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHPWebPage.php to edit this template
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>

        <?php
        //colocar un formulario que va a introducir 3 botones
        //si en lugar de numero1,2,3 SOLO EN NAME ponemos corchetes y lo pasamos a una variable tipo array que se recoge en 5.salida
        ?>

        <form action="5salida.php"> 

            <div>
                <label for="numero1">
                    numero1
                </label>
                <input type="number" name="numeros[]" id="numero1" >
            </div>
               <div>
                <label for="numero2">
                    numero2
                </label>
                <input type="number" name="numeros[]" id="numero2" >
            </div>
               <div>
                <label for="numero3">
                    numero3
                </label>
                <input type="number" name="numeros[]" id="numero3" >
            </div>
            <button>calcular</button>
        </form>
    </body>
</html>
