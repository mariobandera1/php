<?php

//funcion promedio de varios numeros 
//se puede poner con func get args o con los 3 puntos (entre los parentesis de promedio arriba) aqui va con func_get_args

function promedio() {
    $numeros = func_get_args();
    $result = 0;
    $suma = 0;
    $cantidad = count($numeros);
  //  foreach ($numeros as $numero) {



    //    $suma += $numero;
    //}

    //extiste una funcion que suma un array que es array_sum, 
    
    //esta instruccion hace todo $suma=array_sum($numeros)/count($numeros);
    
    $suma= array_sum($numeros);
    

    $result = $suma / $cantidad;
    return $result;
}
?>
<!DOCTYPE html>
<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHPWebPage.php to edit this template
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        echo promedio(2, 4, 6, 5, 20, 8, 14);
        echo "<br>";
        echo promedio(2, 6, 8, 14);
        ?>
    </body>
</html>
