<?php
//definir la funcion (guardarla) pero no poner nombres de funciones ya reservadas por php siempre en minusculas y sin caracteres especiales
// en realidad se llaman subrutinas, conjunto de instrucciones a las que se da un nombre para repetir algo, si devuelve algo es una funcion,
// si no devuelve nada, sera un procedimiento, en otros lenguajes si es necesario diferenciarlo, por eso por ejemplo en java cuando se hace una funcion
// hay que poner return.
// cuando se llama a la funcion, ejecuta el codigo que tenga dentro si pongo /** se va a añadir a la documentacion del proyecto, por eso hay que documetar la funcion
// no debe confundirse con /*

/**
 * imprime en pantalla un texto, esta funcion NO DEVUELVE NADA, solo imprime, 
 */
function sumar() {

    echo "<div>";
    echo "sumando";
    echo "</div>";
}

//esta funcion va a devolver valor, por ello creo una variable que sera LOCAL A LA FUNCION CREADA, NO PUEDE UTILIZARSE FUERA DE LA FUNCION.

/**
 * funcion que retorna un texto
 * @return string
 */
function sumar1() {
    $mensaje = ""; // variable local el .= hace que la variable vaya añadiendo lo que valía antes mas lo que se va añadiendo
    $mensaje .= "<div>"; // $mensaje = $mensaje . "<div>"
    $mensaje .= "sumando"; // $mensaje = $mensaje . "sumando"
    $mensaje .= "</div>"; // $mensaje = $mensaje . "</div>"
    return $mensaje;     // retorna la variable, al documentar, nos marca automaticamente @return
}

//crea una funcion que va a tomar la variable numero1 y numero2 y lo guarda en $salida, numero1 y numero2 son los parametros.
/**
 * devuelve la suma de dos numeros
 * @param int $numero1 primer numero
 * @param int $numero2 segundo numero
 * @return int
 */
function sumar2($numero1,$numero2){
    $salida="<div>";
    $salida.=($numero1+$numero2); // lo hacemos en tres pasos concatenando en lineas diferentes, pero puede hacerse todo en una
    $salida.="</div>";
    return $salida;
            
}

/**
 * funcion que suma dos numero
 * @param int $numero1
 * @param int $numero2
 * @param string $contenedor es la etiqueda que contiene el resultado de la suma
 */
function sumar3($numero1,$numero2,$contenedor){
    
    $salida="<" . $contenedor . ">";
    $salida .= ($numero1 + $numero2);
    $salida .= "</" .$contenedor . ">";
    
    return $salida;
}
?>
<!DOCTYPE html>
<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHPWebPage.php to edit this template
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        //llamar a la funcion
        sumar();
        sumar();
        sumar();
        echo sumar1();
        echo sumar1();
        
       $s=sumar2(3,5);// graba en la variable lo que devuelve la funcion, y luego se imprime la variable con echo
       echo $s;
       
        $s=sumar2(55,67);// podemos llamar a la funcion las veces que queramos
       echo $s;
        
        echo sumar3(4, 9, "h1");
        ?>
    </body>
</html>
