<?php
//paso de parametro por valor, aqui $p no cambia fuera de la funcion
function ver($p){
    
    var_dump($p);
    $p=11;
    var_dump($p);
}

?>
<!DOCTYPE html>
<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHPWebPage.php to edit this template
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $global1=10;
        ver($global1);
        var_dump ($global1);
        ?>
    </body>
</html>
