<?php
//paso de argumentos por referencia 
function ver(&$p){
    
    var_dump($p);
    $p=11; //al cambiar el valor de $p, tambien cambia el valor de $global1
    var_dump($p);
}

?>
<!DOCTYPE html>
<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHPWebPage.php to edit this template
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $global1=10;
        ver($global1);
        var_dump ($global1);
        ?>
    </body>
</html>
