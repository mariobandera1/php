<?php

// funcion sumar con numero de argumentos variable metodo nuevo, usar este.
//lo mas comodo es pasar un array per primero , se ponen 3 puntos y la variable

/**
 * suma los numeros pasados como argumentos
 * @param int $numeros listas de numeros enteos
 * @return int suma los numeros
 */
function sumar(...$numeros) {
    $resultado = 0;
    //foreach ($numeros as $numero){
    //  $resultado+=$numero;  //$resultado=$resultado+$numero
    //}

    for ($c = 0; $c < count($numeros); $c++) {
        $resultado += $numeros[$c];
    }
    return $resultado;
}
?>

<!DOCTYPE html>
<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHPWebPage.php to edit this template
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        echo sumar(1, 2, 3, 4, 5, 6); //devuelve un array de 6 posiciones

        echo "<br>";

        echo sumar(1, 2); //devuelve un array de 2 posiciones
        ?>
    </body>
</html>
