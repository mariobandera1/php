<?php
// metodo antiguo con la funcion func_get_args devuelve los numeros en formato array pero mejor el sistema nuevo
function sumar(){
    $numeros = func_get_args();
    $resultado=0;
    foreach ($numeros as $numero){
        $resultado+=$numero;  //$resultado=$resultado+$numero
    }
    //con un for
   // for ($c = 0; $c < count($numeros); $c++) {
      //  $resultado+$numeros[$c];
        
   // }
    return $resultado;
}

?>

<!DOCTYPE html>
<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHPWebPage.php to edit this template
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
       echo sumar (1,2,3,4,5,6); //devuelve un array de 4 posiciones
       
       echo "<br>";
       
        echo sumar (1,2); //devuelve un array de 2 posiciones
        ?>
    </body>
</html>
