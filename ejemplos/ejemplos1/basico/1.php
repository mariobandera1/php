
<?php
// contenido de php
// modificar la web y colocar otro div con un parrafo
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Ejemplo</title>
    </head>
    <body>
        <!-- Primer contenedor -->
        <div>
            Contenedor universal
        </div>

        <!-- Creamos otro contenedor -->
        <div>
            <p>
                Contenedor de parrafo
            </p>
            <p>
                Otro parrafo
            </p>
        </div>
        
        <div>
            <p>Texto</p>
        </div>
        
        <?php
            echo "<div>";
            echo "<p>texto</p>";
            echo "</div>";
        ?>
    </body>
</html>