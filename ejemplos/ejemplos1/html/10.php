<!DOCTYPE <html>
<!--  

el div este va: por etiqueta

con una altura de 100px:height:100px
con un tamaño de letra de 40px:font-size:40px
con un color de letra blanco:color:white
con el color de fondo negro:background-color:black

este es un selector por etiqueta 
pero los estilos los carga desde una hoja de estilos en este caso es 1.css


-->
</html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="css/1.css">
    </head>
    <body> 
        <div>
            ejemplo de clase
        </div>
        <div>
            otro texto
        </div>
        </body>
</html>
