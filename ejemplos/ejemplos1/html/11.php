<!DOCTYPE html>
<!--
colocar una tabla de 3x3.

Las celdas de las 4 esquinas las quiero en color con fondo negro
    background-color:black
todas las celdas van con borde rojo
  border:1px solid red ;

aplicarlo utilizando una hoja de estilos

con lo cual creamos un estilo para todos los tr con el borde rojo y que en las esquinas creamos una clase y se ponen a las que correspondan.

-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="css/2.css">
    </head>
    
    <body>
        <table> 
            <tr>
                <td class="esquinas">1</td>
                <td>2</td>
                <td class="esquinas">3</td>
            <tr>
                <td>4</td>
                <td>5</td>
                <td>6</td>
            </tr>

            <tr>
                <td class="esquinas">7</td>
                <td>8</td>
                <td class="esquinas">9</td>
            </tr>
    </body>
</html>
