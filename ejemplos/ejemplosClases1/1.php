<?php

//cargar fichero
require_once './clases/Persona.php';
//instanciar el objeto
$objeto=new Persona();

//poniendo la altura
$objeto->altura=180;
$objeto->nombre="Eva";
//ejecutando el metodo hablar
echo $objeto->hablar();

//depurando el objeto
var_dump($objeto);

echo $objeto->presentar();


echo Persona::presentar();