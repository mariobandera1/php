<?php


class Persona {
    //propiedades
    public $altura=0;
    
    public $nombre="";
    
    //metodo
    public function hablar(){
        return $this->nombre."bla bla bla";
        
            
        }
        //metodo estatico, es el metodo que puedo utilizar sin instanciar objetos
        public static  function presentar(){
            //en un metodo estatico no puedo hacer referencia a elementos con $this
            return "hola";
    }
}
