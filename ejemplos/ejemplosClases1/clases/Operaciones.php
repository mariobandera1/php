<?php

class Operaciones {
    
    public $numero1=0;
    public $numero2=0;
    
    //metodo sumar, no estatico
    
    public function sumar(){
        
        return $this->numero1 + $this->numero2;
               
        
    }
    //metodo estatico
    public static function sumarEstatico($a,$b){
        return $a+$b;
        
    }
}