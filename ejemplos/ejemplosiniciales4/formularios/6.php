<!DOCTYPE html>
<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHPWebPage.php to edit this template
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>


        <?php
        // $resultado="";

        //inicializamos las variables.
        
        $numero1="";
        $numero2="";
        $resultado="";

        //comprobar si existe el boton, es decir, si se ha pulsado alguno de los botones

        if (isset($_GET["operacion"])) {
            $numero1 = $_GET["numero1"];
            $numero2 = $_GET["numero2"];
            
            //el switch debe ir dentro el if, porque si no, no lo coge, se sale del if y da error.
            //primero pregunta si se ha pulsado un boton y segundo cual se ha pulsado

            switch ($_GET["operacion"]) {
                case "sumar":
                    $resultado=$numero1 + $numero2;
                    break;
                case "restar":
                    $resultado=$numero1 - $numero2;
                    break;
                case "multiplicar":
                    $resultado= $numero1 * $numero2;
                    break;
                    
                case "dividir":
                    $resultado= $numero1 / $numero2;

                    break;
                //el ultimo no hace falta ponerlo, pero mejor ponerlo por si hubiea que poner mas opciones
            }
            //echo $resultado;
        }
        require './_formulario6.php';
        
        //el require lo ponemos asi teniendo value en el formulario value="", viene aqui porque
        //si no se pulsan los botones no entra en el el if
        ?>
        <div> el total es :<?=$resultado?></div>
    </body>
</html>
