<?php


spl_autoload_register(function ($clase) {
    include 'clases/' . $clase . '.php';
});


$ford=new Coche("ford","mondeo",2000);

$ford->setMarca("Fiat"); // como marca y modelo son privates solo pueden cambiarse con el metodo setter (los metodos creados en la clase)
$ford->setModelo("doblo");
$ford->cilindrada(1000); //cilindrada es pubilca y se puede hacer de dos formas

var_dump($ford);
