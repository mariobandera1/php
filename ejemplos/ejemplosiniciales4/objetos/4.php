<?php

/* crear un objeto de tipo persona, uno de tipo perro y otro de tipo gato.
 */

spl_autoload_register(function ($clase) {
    require  'clases/' . $clase . '.php';
});

$persona1=new Persona();
$persona1->nombre ="pepe"; //si la clase no tiene constructor hay que pasarlo aqui, 

$perro= new Perro("Tarzan");//como tiene contructor puede inicializarse y pasar el argumento directamente
$gato=new Gato("bus", "negro", "si", 10);

//leer las propiedades

    
echo $persona1->nombre;

echo $perro->nombre;

echo "<br>";
echo "mi gato se llama " .$gato->nombre;
echo "<br>";
echo "el color de pelo de mi gato es  " . $gato->color;

echo "<br/>";

$persona1->apellidos="Vazquez";
$persona1->direccion="Vargas1";
$persona1->poblacion="Santander";
$persona1->cp="39005";

echo $persona1->direccion();

$persona2=new Persona();
$persona2->nombre="Ana";
$persona2->direccion="Augusto 3";
$persona2->poblacion="Torrelavega";
$persona2->cp="39002";

echo "<br>";

echo $persona2->nombre;

echo "<br>";

echo $persona2->direccion();



echo "<br>";

$perro2 = new Perro(); // no se puede crear un perro sin nombre porque el constructor lo exige y debe pasarse.

echo "<br>";

echo $perro2->nombre;

$gato1 = new Gato("Dulci");

var_dump($gato1);