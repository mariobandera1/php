<?php

class Persona{
    

    /* miembros de clase (propiedades y metodos)
     * propiedades - unidades que pueden almacenar informacion (como las variables, salvo que pertenecen a la clase) pero se debe indicar la vsibilidad, 
     * si es publica, o privada, si se ve dentro o fuera de la clase, si solo es de la clase se pone private y si se puede acceder desde fuera de la clse 
     * se pone public  ej 
     * /*  public $nombre; private $grado;  tb hay protected pero para eso debe explicarse la herencia
     * metodos son las acciones, funcionan como si fueran la funciones.
    /*
     * 
     */
    public $nombre; 
     
     private $grado;
    
     
    /**/

    public function hablar(){
    return "bla bla bla";
    
    }
    
    public function hablarAlto(){
     return $this->chillar();    // llamar a un miembro desde dentro de la clase, el metodo chillar es privado y solo se puede desde  otro metodo
    
    }
    private function chillar(){
        return " ahahahahah";
    }
}

/* crear un objeto */

$persona1=new Persona();

$persona1->nombre="Pepe";  // escribiendo un valor en nombre
echo $persona1->nombre;   // accediendo al nombre
echo $persona1->hablar(); //llamando al metodo hablar

echo $persona1->hablarAlto();
     