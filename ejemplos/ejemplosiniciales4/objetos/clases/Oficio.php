<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Oficio
 *
 * @author Mario
 */
class Oficio {
    public $nombre;
   public  $salarioBase;
   public $horasSemanales;
   
   
   public function __construct($nombre, $salarioBase, $horasSemanales) {
       $this->nombre = $nombre;
       $this->salarioBase = $salarioBase;
       $this->horasSemanales = $horasSemanales;
   }
   
   public function calcular() {
       
       $calculo = $this->salarioBase*$this->horasSemanales;
       
       return $calculo;
       
   }

    //put your code here
}
