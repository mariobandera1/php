<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Coche
 *
 * @author Mario
 */
class Coche {
    private $marca;
    private $modelo;
    public $cilindrada;

    public function __construct($marca, $modelo, $cilindrada) {
        $this->marca = $marca;
        $this->modelo = $modelo;
        $this->cilindrada = $cilindrada;
    }
    
    public function setMarca($marca): void {
        $this->marca = $marca;
    }

    public function setModelo($modelo): void {
        $this->modelo = $modelo;
    }

    public function setCilindrada($cilindrada): void {
        $this->cilindrada = $cilindrada;
    }

    
    //los metodos setter son para inicializar 
}
