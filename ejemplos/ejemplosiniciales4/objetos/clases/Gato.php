<?php


class Gato {
    
    public $nombre;
    public $color;
    public $pelo;
    public $peso;
    //put your code here
    
    public function __construct($nombre="", $color="negro", $pelo="si", $peso=5) { // creamos un constructor con unos datos por defecto.
        $this->nombre = $nombre;
        $this->color = $color;
        $this->pelo = $pelo;
        $this->peso = $peso;
    }


}
