<?php

/**
 * clase para crear un trabajo asociado a una persona
 * 
 * @param Humano $persona persona
 * @param Oficio $oficio oficio de la persona
 */
class Trabajan {
    public $persona;  // esto tiene que ser un objeto de tipo Humano
    public $oficio;   //esto tiene que ser un objeto de tipo Oficio
    
    public function __construct(Humano $persona , Oficio $oficio="") {
        $this->persona = $persona;
        $this->oficio = $oficio;
    }

}
