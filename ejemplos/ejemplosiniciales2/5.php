<?php

$numero = 22;

//comprobar si el numero es par o impar

if ($numero % 2 == 0) {
    echo "es par";
} else {
    echo "es impar";
}

//solucion 2

$salida = "Es impar";

if ($numero % 2 == 0) {

    $salida = "es par";
}

//solucion 3

if ($numero % 2 == 0) {
    $salida = "es par";
} else {
    $salida = "es impar";
}

echo$salida;

//solucion 4 cuando devuelve cero, en informatica se entiende que es falso,
// si es distinto de cero es verdadero.

if ($numero%2) {
    $salida="es impar";
    
} else {
    $salida="es par";
}