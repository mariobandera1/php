<?php
/*
 * Heredoc se ponen 3 menores <<<  y un label (DATOS en este caso) , hasta que se ponga DATOS; 
 * se puede poner todo en html+php
 sin necesidad de comillas
 * nos permite integrar php y html sin tener que poner comillas
 * 
*/



$nombre="Ana";
$edad=20;

//heredoc
$salida=<<<DATOS
        <div>$nombre</div>
        <div>$edad</div>
                
DATOS;

//$salida="<div>$nombre</div><div>$edad</div>";

echo $salida;

