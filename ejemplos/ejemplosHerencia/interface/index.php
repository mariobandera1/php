<?php

spl_autoload_register(function ($nombre_clase) {
    include "./clases/" . $nombre_clase . '.php';
});

// new Persona(); no se puede instanciar un inteface.

$usuario1=new Usuario();

var_dump($usuario1);

echo $usuario1->hola();

echo "<br>";

$empleado1=new Empleado(); //clase que hereda de los dos interfaces

echo $empleado1->hola();
echo "<br>";
echo $empleado1->calcularSueldo();

echo "<br>";
$empleado2= new EmpleadoTodo();

echo $empleado2->salir();

var_dump($empleado2);
