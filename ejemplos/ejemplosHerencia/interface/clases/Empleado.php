<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Empleado
 *
 * @author Mario
 */
class Empleado implements Trabajador, Persona {//creamos una clase de dos interfaces distintos, de este modo
    
    public function adios(): string {           //hay que poner todos los metodos de cada interface
        return "adios";
    }

    public function hola(): string {
     return "hola";   
    }

    public function calcularSueldo(): float {
        return 1000.00;
    }

    public function mostrarInformacion(): string {
        return "mis datos";
    }

    //put your code here
}
