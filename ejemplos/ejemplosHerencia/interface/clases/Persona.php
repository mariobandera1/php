<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Persona
 *
 * @author Mario
 */
interface Persona {
    
  // public string $nombre; NO PUEDEN TENER PROPIEDADES LOS INTERFACES, SOLO PUEDEN TENER METODOS.
    
    public function hola():string; //EN UN INTERFACE TODOS LOS METODOS DEBEN SER PUBLICOS, SI SE PONE PRIVADO->ERROR
      
      public function adios ():string;
    

         
    
    //put your code here
}
