<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Usuario
 *
 * @author Mario
 */
class Usuario implements Persona { //se hace como una clase pero con implements, del interface Persona
    
    public function adios(): string { //necesario "implementar" los metodos definidos en el 
        
        return "hasta la vista";
    }

    public function hola(): string {
        
        return "bienvenidos";
        
    }

    //put your code here
}
