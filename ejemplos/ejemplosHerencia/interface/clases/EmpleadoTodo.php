<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of EmpleadoTodo
 *
 * @author Mario
 */
class EmpleadoTodo implements Todo{ //creamos una clase con el interfaz todo (que es a la vez un interfaz de otros dos interfaces)
    
    public function adios(): string {
        return "adios";
        
    }

    public function hola(): string {
        return "holahola";
    }

    public function salir(): string {
        return "saliendo";
    }

    public function calcularSueldo(): float {
      return 10000.0;   
    }

    public function mostrarInformacion(): string {
        return "informacion";
    }

    
    
    //put your code here
}
