<?php

spl_autoload_register(function ($nombre_clase) {
    include "./clases/" . $nombre_clase . '.php';
});

//new Base(); // las clases abstractas no pueden instanciarse


$jorge=new Cliente(); //crear objeto de tipo cliente

var_dump($jorge);

echo $jorge->hablar();//llamo al metodo hablar, aunque no esta inicializado, esta en la clase Base

$silvia = new Humano("silvia", 30); //creamos el objeto silvia de clase Humano, hay que pasar argumentos

var_dump($silvia);

echo $silvia->saludar(); //la propiedad tipo es privada y no se puede acceder si no es desde el setter/getter, ni desde los hijos
                           // para que a una propiedad se pueda acceder desde las sublclases debe ser protected.

echo "<br>";

$bigotes = new Perro();

var_dump($bigotes);

echo $bigotes->saludar();