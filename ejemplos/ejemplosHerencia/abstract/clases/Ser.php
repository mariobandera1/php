<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Ser
 *
 * @author Mario
 */
abstract class Ser {
    //put your code here
    protected string $tipo;
    
    public function getTipo(): string {
        return $this->tipo;
    }

    public function setTipo(string $tipo): void {
        $this->tipo = $tipo;
    }
    
    abstract public function saludar(): string; //al tener los metodos abstractos, los hijos deben tenerlos
    
    abstract public function despedir():string;


    
}
