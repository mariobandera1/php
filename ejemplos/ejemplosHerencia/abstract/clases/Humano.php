<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Humano
 *
 * @author Mario
 */
class Humano extends Ser {
    
    private string $nombre;
    private int $edad;
    
    public function despedir(): string {
        return "adios";
    }

    public function saludar(): string {
        return "hola";
    }
    
    public function getNombre(): string {
        return $this->nombre;
    }

    public function getEdad(): int {
        return $this->edad;
    }

    public function setNombre(string $nombre): void {
        $this->nombre = $nombre;
    }

    public function setEdad(int $edad): void {
        $this->edad = $edad;
    }

        
    public function __construct(string $pnombre, int $pedad) { //aqui se ponen los argumentos en el constructor
        $this->tipo ="Ser Humano"; //tipo es heredado de Ser
        $this->nombre=$pnombre; //aqui el nombre que se pasa por argumento se graba en la propiedad nombre
        $this->edad=$pedad;     //como las propiedades son privadas, hay que acceder a traves del getter y setter
    }

    //put your code here
}
