<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Perro
 *
 * @author Mario
 */
class Perro extends Ser{
    
    public function despedir(): string { //obligatorio al ser abstracto en la clase padre (ser)
        
        return "Guauguau";
        
    }

    public function saludar(): string { //obligatorio al ser abstracto en la clase padre (ser)
        
        return "guauguaguguguuguguguuau";
        
    }

    public function __construct() {
        
        $this->tipo="perrito";
        
    }

    //put your code here
}
