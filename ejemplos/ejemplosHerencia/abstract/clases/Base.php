<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Base
 *
 * @author Mario
 */
abstract class Base { //no se pueden instanciar objetos de una clase abstracta,
    
    public string $nombre;
    public string $fechaNacimiento;
    public string $apellidos;
    
    public function hablar(): string{
        
        return "bla bla";
        
    }
    
    abstract public function Presentacion (): string; // no se puede rellenar,aqui, pero en el hijo debe OBLIGATORIAMENTE SOBREESCRIBIRSE
        
        //pero si pongo el tipo de dato, en el hijo debe devolver ese tipo de dato.
    
    
    //put your code here
}
