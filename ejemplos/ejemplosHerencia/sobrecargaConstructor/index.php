<?php

spl_autoload_register(function ($nombre_clase) {
    include "./clases/" . $nombre_clase . '.php';
});

$persona1 = new Persona("EVA", "M", 40);

var_dump($persona1);

$persona2 = new Persona();

var_dump($persona2);

$persona3 = new Persona("EVA");

var_dump($persona3);

$persona1 = new Persona1("EVA", "M", 40);

var_dump($persona1);

$persona2 = new Persona1();

var_dump($persona2);

$persona3 = new Persona1("EVA");

var_dump($persona3);

$persona1 = new Persona2(28, "pepe", "H");

var_dump($persona1);

$persona2 = new Persona2();

var_dump($persona2);

$persona3 = new Persona2("EVA");

var_dump($persona3);

$persona1 = new Persona3([
    "edad" => 25,
    "nombre" => "ramon",
    "sexo" => "H"
        ]);

var_dump($persona1);

$persona2 = new Persona3([]);

var_dump($persona2);

$persona3 = new Persona3([
    "nombre" => "eva",
    "nombrecompleto" => "pepe", //este no lo coge al no cogerlo con la interseccion de arrays.
    "edad" => 25,
    "sexo" => "M"
        ]);

var_dump($persona3);

$persona1 = new Persona4([
    "edad" => 25,
    "nombre" => "ramon",
    "sexo" => "H"
        ]);

var_dump($persona1);

$persona2 = new Persona4([]);

var_dump($persona2);

$persona3 = new Persona4([
    "nombre" => "Antonio",
    "nombrecompleto" => "luis", //este no lo coge al no cogerlo con la interseccion de arrays.
    "edad" => 25,
    "sexo" => "M"
        ]);

var_dump($persona3);
