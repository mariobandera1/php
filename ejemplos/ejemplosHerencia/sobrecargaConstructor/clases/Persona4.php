<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Persona
 *
 * @author Mario
 */
class Persona4 {

    public ?string $nombre = null; //si un string se inicializa a null, hay que poner la interrogacion delante del tipo
    public string $sexo = "H";
    public int $edad = 0;

    public function __construct(array $datos = []) {

        //inicio todas las propiedades de la clase con el array pasado

        foreach ($datos as $indice => $valor) {

            if (property_exists("Persona4", $indice)) {
                $this->$indice = $valor;
            }
        }
    }

}
