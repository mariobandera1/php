<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Persona
 *
 * @author Mario
 */
class Persona3 {

    public ?string $nombre = null; //si un string se inicializa a null, hay que poner la interrogacion delante del tipo
    public string $sexo = "H";
    public int $edad = 0;

    public function __construct(array $datos = []) {
        //creamos un array asociativo con los valores por defecto de las propiedades
        //esta bien pero es mejor hacerlo a traves de la funcion get_class-vars
        /*  $inicial=[
          "nombre"=>$this->nombre,
          "sexo"=>$this->sexo,
          "edad"=>$this->edad,


          ];
         * $inicial, es para quedarse con los indices que han pasadoal constructor que existan como propiedad
         */

        $inicial = get_class_vars("persona3");

        var_dump($inicial);

        $final = array_intersect_key($datos, $inicial);
        //inicio todas las propiedades de la clase con
        //el array pasado


        foreach ($final as $indice => $valor) {
            $this->$indice = $valor;
        }
    }

    /*  $final=$datos;

      "nombre"=>$this->nombre=$datos["nombre"];
      "sexo"=>$this->sexo=$datos["sexo"];
      "edad"=>$this->edad=$datos["edad"];
     * 
     * 
     */
}
