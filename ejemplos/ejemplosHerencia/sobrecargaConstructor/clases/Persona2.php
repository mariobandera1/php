<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Persona2
 *
 * @author Mario
 * 
 * sobrecargar el constructor
 * sin parametros creo una paersona con los valores por defecto
 * con 1 parametro creo una persona con el nombre
 * con 2 parametros creo una persona con sexo y nombre
 * con 3 parametros creo una persona con edad, nombre y sexo
 */
class Persona2 {

    public ?string $nombre = null; //si un string se inicializa a null, hay que poner la interrogacion delante del tipo
    public string $sexo = "H";
    public int $edad = 0;

    public function __construct(...$datos) {

        $numero = count($datos);

        $nombre = "__construct{$numero}";

        if (method_exists($this, $nombre)) {

            //  call_user_func_array([$this,$nombre], $datos);

            $this->$nombre(...$datos); // puede utilizarse este, o el anterior indistintamente
        }
    }

    public function __construct0() {
        
    }

    public function __construct1(string $nombre) {

        $this->nombre = $nombre;
    }

    public function __construct2(string $sexo, string $nombre) {

        $this->nombre = $nombre;
        $this->sexo = $sexo;
    }

    public function __construct3(int $edad, string $nombre, string $sexo) {
        $this->nombre = $nombre;
        $this->sexo = $sexo;
        $this->edad = $edad;
    }

    //put your code here
}
