<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Persona
 *
 * @author Mario
 */
class Persona1 {
    
    public ?string $nombre=null; 
    public string $sexo="H";
    public int $edad=0;
    
    //voy a realizar la sobrecarga del constructor con parametros por defecto
    
    public function __construct(?string $nombre=null, string $sexo="H", int $edad=0) {
        $this->nombre = $nombre;
        $this->sexo = $sexo;
        $this->edad = $edad;
    }

}
