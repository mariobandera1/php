<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Persona
 *
 * @author Mario
 */
class Persona {

    public ?string $nombre = null; //si un string se inicializa a null, hay que poner la interrogacion delante del tipo
    public string $sexo = "H";
    public int $edad = 0;

    public function __construct(...$datos) {

        $numero = count($datos);

        $nombre = "__construct{$numero}";

        if (method_exists($this, $nombre)) {

            //  call_user_func_array([$this,$nombre], $datos);

            $this->$nombre(...$datos); // puede utilizarse este, o el anterior indistintamente
        }
    }

    public function __construct0() {
        
    }

    public function __construct1(?string $nombre) {

        $this->nombre = $nombre;
    }

    public function __construct2(?string $nombre, string $sexo) {

        $this->nombre = $nombre;
        $this->sexo = $sexo;
    }

    public function __construct3(?string $nombre, string $sexo, int $edad) {
        $this->nombre = $nombre;
        $this->sexo = $sexo;
        $this->edad = $edad;
    }

    //put your code here
}
