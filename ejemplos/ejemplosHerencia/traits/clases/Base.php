<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPTrait.php to edit this template
 */

/**
 *
 * @author Mario
 */
trait Base {
    
    public string $nombre;
    public string $fechaNacimiento;
    public string $apellidos;
    //put your code here
    
    public function hablar(string $texto) {
        
        return "digo {$texto}";
        
    }
    
    public function __constructBase(string $nombre, string $fechaNacimiento, string $apellidos) {//cambiamos el construct de los traits por otro nombre para que no de conflicto con el de la clase
                    
        $this->nombre = $nombre;
        $this->fechaNacimiento = $fechaNacimiento;
        $this->apellidos = $apellidos;
    }

}
