<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPTrait.php to edit this template
 */

/**
 *
 * @author Mario
 */
trait Direccion {
    
    public string $tipo;
    public int $numero;
    public string $bloque;
    public string $puerta;
    public string $poblacion;
    private int $cp; //hacemos a esta propiedad private, un getter y un setter
    
    public function __constructDireccion(string $tipo, int $numero, string $bloque, string $puerta, string $poblacion) {
        $this->tipo = $tipo;
        $this->numero = $numero;
        $this->bloque = $bloque;
        $this->puerta = $puerta;
        $this->poblacion = $poblacion;
    }
    public function getCp(): int {
        return $this->cp;
    }

    public function setCp(int $cp): void {
        $this->cp = $cp;
    }

        
    //put your code here
}
