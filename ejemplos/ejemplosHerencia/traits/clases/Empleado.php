<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Empleado
 *
 * @author Mario
 */
class Empleado  { //va a tener todo lo que tiene base y direccion (que son traits)no se puede con extends sino con use

    use Base,Direccion;
    
    public float $sueldo;  
    
    
    public function __construct(string $nombre,float $sueldo,string $poblacion) {
        
        $this->__constructBase($nombre,"","");//no les llamamos construct porque da conflicto se llaman aqui distintos
        $this->__constructDireccion("", 1, "", "", $poblacion);
        $this->sueldo = $sueldo;
    }

    //put your code here
}
