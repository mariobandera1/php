<?php

spl_autoload_register(function ($nombre_clase) {
    include "./clases/" . $nombre_clase . '.php';
});

$jorge = new Empleado("jorge",1000,"santander"); //objeto implementado de la clase empleado, que se obtiene de los traits Base y Direccion

var_dump($jorge);

$casa1 =new TipoA();
var_dump($casa1);

$casa2=new TipoB();
 
var_dump($casa2);