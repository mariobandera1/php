<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Div
 *
 * @author Mario
 */

namespace clases\texto;  //asi puedo tener las clases en distintas carpetas



class Div_1 {

    //put your code here
    private string $texto = "";
    private ?int $ancho;
    private ?int $altura;
  
    
    //const PLANTILLA = "clases\texto\PlantillaDiv.php";

    public function __construct(string $texto, ?int $ancho = null, ?int $altura = null) {
        $this->texto = $texto;
        $this->ancho = $ancho;
        $this->altura = $altura;
    }

    public function __toStringNormal() {
        $resultado = "<div style=\"border:1px solid black;";  ///creacion de la etiqueta por partes, "importante"
        $resultado .= $this->getAncho();
        $resultado .= $this->getAltura();
       $resultado .= "\">";
        $resultado .= $this->texto;

        $resultado .= "</div>";

        return $resultado;
    }

    public function getAncho(): string {  //ojo hay que poner string porque es lo que va a devolver a la etiqueta, aunque $ancho sea int
        if(is_null($this->ancho)) {

            return "";   //con este if hacemos para que no haga nada si es null y que entre a la operacion si no lo es
        } else {

            $resultado = "width:{$this->ancho}px;";
        }
        return $resultado;
        
    //    $resultado=$this->ancho ?? "";   este operador ?? 
    }

    public function getAltura(): string {

        if (is_null($this->altura)) {
            
            return "";
        } else {
            $resultado = "height:{$this->altura}px;";
        }

        return $resultado;
    }
    
    public function __toString() {
        
        $fichero= file_get_contents("clases/texto/PlantillaDiv.php");//hay que poner la ruta exacta donde esta la plantilla
        
        $borde="border:5px solid black;";
        $ancho=$this->getAncho();
        $alto=$this->getAltura();
        
        $contenido=$this->texto;
        $estilo=$borde.$ancho.$alto; //juntamos concatenadamente y lo ponemos en estilo para luego cambiarlo por la etiqueta STYLE con el str_replace
              
        
        /*
        return str_replace(   //funcion muy importante sustituye un texto por otro, o un array por otro
                ["{{STYLE}}",
                    "{{TEXTO}}",
                    "{{width}}",  //array a sustituir
                    "{{height}}"
                ],
                
                [//$this->getSrc(),
                   // $this->getBorder(),
                   // $this->getAncho(),    //sustituimos por los metodos
                   // $this->getAlto()
                ],
                
                $resultado  //con un fichero aparte
                //self::IMG   aqui si no lo hacemos con un fichero aparte
         
        );
         * */
        
        return str_replace(
               [ "{{STYLE}}","{{TEXTO}}"],
                [$estilo,$contenido],
                $fichero);
                
                
                
                
                
                
                
        
        return $fichero;
         

}

}