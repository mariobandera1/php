<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Div
 *
 * @author Mario
 */

namespace clases\texto;  //asi puedo tener las clases en distintas carpetas

class Div {

    //put your code here
    private string $texto = "";
    private ?int $ancho;
    private ?int $altura;

    public function __construct(string $texto, ?int $ancho = null, ?int $altura = null) {
        $this->texto = $texto;
        $this->ancho = $ancho;
        $this->altura = $altura;
    }

    public function __toString() {
        $resultado = "<div style=\"border:1px solid black;";  ///creacion de la etiqueta por partes, "importante"
        $resultado .= $this->getAncho();
        $resultado .= $this->getAltura();
        $resultado .= "\">";
        $resultado .= $this->texto;

        $resultado .= "</div>";

        return $resultado;
    }

    public function getAncho(): string {  //ojo hay que poner string porque es lo que va a devolver a la etiqueta, aunque $ancho sea int
        if(is_null($this->ancho)) {

            return "";   //con este if hacemos para que no haga nada si es null y que entre a la operacion si no lo es
        } else {

            $resultado = "width:{$this->ancho}px;";
        }
        return $resultado;
        
    //    $resultado=$this->ancho ?? "";   este operador ?? 
    }

    public function getAltura(): string {

        if (is_null($this->altura)) {
            
            return "";
        } else {
            $resultado = "height:{$this->altura}px;";
        }

        return $resultado;
    }

}

//<div style="width:100px";borde:1px solid black>a,aquieltexto</div>  --- quiero crear estas etiquetas automaticamente