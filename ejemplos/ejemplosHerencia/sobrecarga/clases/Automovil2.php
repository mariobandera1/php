<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */

class Automovil2 {

    public int $potencia = 1;
    public int $velocidad = 0;

       

       
    

    public function acelerar(...$datos) {

         $numero = count($datos);
        $nombre = "acelerar{$numero}";
        if (method_exists($this, $nombre)) {
             $this->$nombre(...$datos);
        }
    }

    private function acelerar1(int $incr): void {

        $this->velocidad = $this->velocidad + $incr;
    }

    private function acelerar2($incr, $pot): void {

        $this->velocidad = $this->velocidad + ($incr + $this->potencia * $pot);
    }

    //put your code here
}
