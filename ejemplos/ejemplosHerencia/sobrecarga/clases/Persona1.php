<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Persona1
 *
 * @author Mario
 */
class Persona1 {

    public ?string $nombre = null; //si un string se inicializa a null, hay que poner la interrogacion delante del tipo
    public string $sexo = "H";
    public int $edad = 0;

    public function __call($name, $arguments) { //cuando se llame a un metodo que no exista, se llama al metodo magico call
        
        $numero = count($arguments); // se guarda en $numero,el numero de argumentos que hay en argumentos
        
        $nombre = "{$name}{$numero}";
        
        if (method_exists($this, $nombre)) { //comprobando que el metodo existe.
            //utilizando la funcion call_user_func_array
            return call_user_func_array([$this, $nombre], $arguments);
            
            
            //utilizando una funcion variable
           //return $this->$nombre(...$arguments);//al poner los tres punto, convierte el array en variables sueltas
        }


        var_dump($arguments);
    }

    private function saludar0() {

        return "hola";
    }

    private function saludar1($a1) {

        return "hola  {$a1} yo soy {$this->nombre}";
    }

    private function saludar2($a1, $a2) {

        return "hola {$a1} yo soy {$this->nombre}. veo que tienes {$a2} años, yo tengo {$this->edad} ";
    }

    //put your code here
}

//put your code here

