<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Persona2
 *
 * @author Mario
<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Persona
 *
 * @author Mario
 */
class Persona2 {
    
    public ?string $nombre=null; //si un string se inicializa a null, hay que poner la interrogacion delante del tipo
    public string $sexo="H";
    public int $edad=0;
    
    
    
    
    public function saludar(...$datos){
        
        $numero = count($datos);
        $nombre = "saludar{$numero}";
        if (method_exists($this, $nombre)) {
            return $this->$nombre(...$datos);
        }
        /*      
        if (count($datos)==0) {
           return $this->saludar0();
        }
        
        if (count($datos)==1) {
           return $this->saludar1($datos[0]);
        }
        if (count($datos)==2) {
          return  $this->saludar2(...$datos);
            
        }
        //var_dump($datos);
  */      
        
        
    }
    
    private function saludar0(){
        
        return "hola ";
        
    }
    
    private function saludar1($a1){
        
        return "hola  {$a1} yo soy {$this->nombre}";
        
    }
    
    private function saludar2($a1,$a2){
        
        return "hola {$a1} yo soy {$this->nombre}. veo que tienes {$a2} años, yo tengo {$this->edad} ";
    }
    
    //put your code here
}
