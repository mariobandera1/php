<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Automovil
 *
 * @author Mario
 */
class Automovil {

    public int $potencia = 1;
    public int $velocidad = 0;

    public function acelerar0(): void {

        $this->velocidad = $this->velocidad + 1;
    }

    public function acelerar1(int $incr): void {

        $this->velocidad = $this->velocidad + $incr;
    }

    public function acelerar2($incr, $pot): void {

        $this->velocidad = $this->velocidad + ($incr + $this->potencia * $pot);
    }

    //put your code here
}

class Automovil1 {

    public int $potencia = 1;
    public int $velocidad = 0;

    public function __call($name, $arguments) { //cuando se llame a un metodo que no exista, se llama al metodo magico call
        $numero = count($arguments); // se guarda en $numero,el numero de argumentos que hay en argumentos

        $nombre = "{$name}{$numero}";

        if (method_exists($this, $nombre)) { //comprobando que el metodo existe.
            //utilizando la funcion call_user_func_array
            return call_user_func_array([$this, $nombre], $arguments);

            //utilizando una funcion variable
            //return $this->$nombre(...$arguments);//al poner los tres punto, convierte el array en variables sueltas
        }


        var_dump($arguments);
    }

    public function acelerar0(): void {

        $this->velocidad = $this->velocidad + 1;
    }

    public function acelerar1(int $incr): void {

        $this->velocidad = $this->velocidad + $incr;
    }

    public function acelerar2($incr, $pot): void {

        $this->velocidad = $this->velocidad + ($incr + $this->potencia * $pot);
    }

    //put your code here
}
