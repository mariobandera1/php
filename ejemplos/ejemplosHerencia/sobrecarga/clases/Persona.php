<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Persona
 *
 * @author Mario
 */
class Persona {
    
    public ?string $nombre=null; //si un string se inicializa a null, hay que poner la interrogacion delante del tipo
    public string $sexo="H";
    public int $edad=0;
    
    
    
    
    public function saludar(){
        
        return "hola";
        
    }
    
    public function saludar1($a1){
        
        return "hola  {$a1} yo soy {$this->nombre}";
        
    }
    
    public function saludar2($a1,$a2){
        
        return "hola {$a1} yo soy {$this->nombre}. veo que tienes {$a2} años, yo tengo {$this->edad} ";
    }
    
    //put your code here
}
