<?php

spl_autoload_register(function ($nombre_clase) {
    include "./clases/" . $nombre_clase . '.php';
});

$p = new Persona();

$p->nombre = "EVA";
$p->sexo = "M";
$p->edad = 25;

var_dump($p);

echo $p->saludar();

echo "<br>";

echo $p->saludar1("ramon");

echo "<br>";

echo $p->saludar2("ramon", 50);

$p1 = new Persona1(); //un nuevo objeto de la clave persona como el metodo correr no existe, llama al metodo magico call

$p1->nombre = "Pepe";
$p1->sexo = "H";
$p1->edad = 40;

echo "<br>";

echo $p1->correr(100, "derecha");   // que pone el nombre puesto al metodo y los argumentos los guarda en un array

echo "<br>";

echo $p1->nadar(); //lo mismo, el metodo nada no existe y tb llama a call, el array esta vacio por no tener argumentos.

echo "<br>";

echo $p1->saludar();

echo "<br>";

echo $p1->saludar("ramon");

echo "<br>";

echo $p1->saludar("ramon", 50);

$a1 = new Automovil();

$a1->velocidad = 0;
$a1->potencia = 1;

echo $a1->velocidad;
echo "<br>";
$a1->acelerar0();
echo "<br>";
echo $a1->velocidad;

echo "<br>";

echo $a1->velocidad;
echo "<br>";
$a1->acelerar1(20);

echo $a1->velocidad;
echo "<br>";
$a1->acelerar2(10, 2);

echo $a1->velocidad;

var_dump($a1);


$a2 = new Automovil1();

$a2->velocidad = 0;
$a2->potencia = 1;

echo $a2->velocidad;
echo "<br>";
$a2->acelerar0();
echo "<br>";
echo $a2->velocidad;

echo "<br>";

echo $a2->velocidad;
echo "<br>";
$a2->acelerar1(20);

echo $a2->velocidad;
echo "<br>";
$a2->acelerar2(10, 2);

echo $a2->velocidad;



 $a2=new Automovil2();
    
    echo $a2->acelerar();
    
    echo $a2->velocidad;
    
    echo "<br>";
    
    echo $a2->acelerar(10);
    
    echo "<br>";
    
    echo $a2->velocidad;
    
    echo "<br>";
    
    echo $a2->acelerar(10,5);
    
    echo $a2->velocidad;