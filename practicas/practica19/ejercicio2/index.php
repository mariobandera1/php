<?php

spl_autoload_register(function ($nombre_clase) {
    include "./clases/" . $nombre_clase . '.php';
});


$alumno=new Persona("2020202020");

var_dump($alumno);

$profesor=new Persona("jorge",50,"h");

var_dump($profesor);

$profesor->setPeso(89);

$profesor->setAltura(180);

echo $profesor->calcularIMC();
echo "<br>";
echo (int)$profesor->esMayorDeEdad();
echo "<br>";
echo $profesor;

$profesor->setDni("20211818");

$profesor->setSexo("X"); //al no ser H o M, se cambia directamente a H
var_dump($profesor);


