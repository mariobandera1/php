<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Persona
 *
 * @author Mario
 */
class Persona {

    public ?string $nombre = null;
    public int $edad = 0;
    public ?string $dni = null;
    public string $sexo;
    public int $peso = 0;
    public int $altura = 0;
    const DEBAJO =-1; 
    CONST IDEAL =0;
    CONST ENCIMA =1;

    //put your code here
    //
    public function __construct(...$datos) { //al ponerlo asi, todos las propiedades se graban en datos como un array
        switch (count($datos)) { //cuantos argumentos has pasado? count cuenta el array
            case 0:
                break;
            case 1:

                $this->__construct1($datos[0]); //quiero el nombre que se guarda en la posicion 0

                break;
            case 3:
                $this->__construct3($datos[0], $datos[1], $datos[2]); // al ser otros, se guarda en las posiciones correspondientes


                break;

            default;
        }
    }

    private function __construct1(string $dni) {

        $this->setDni($dni); //en el setter ya genera la letra del DNI
    }

    private function __construct3(string $nombre, int $edad, string $sexo) {

        $this->nombre = $nombre;
        $this->edad = $edad;
        $this->setSexo($sexo); //el sexo lo comprueba el setter
    }
    
    public function calcularIMC ():int{
        
        $imc= $this->peso/($this->altura/100)**2;
        
        if ($imc<20) {
            return self::DEBAJO;
            
        } elseif ($imc<=25) {
            return self::IDEAL;
            
        } else {
            
            return self::ENCIMA;
        }
        
        
       //creo getters y setters    
        
        
        
    }
    public function getNombre(): ?string {
        return $this->nombre;
    }

    public function getEdad(): int {
        return $this->edad;
    }

    public function getDni(): ?string {
        return $this->dni;
    }

    public function getSexo(): string {
        return $this->sexo;
    }

    public function getPeso(): int {
        return $this->peso;
    }

    public function getAltura(): int {
        return $this->altura;
    }

    public function setNombre(?string $nombre): void {
        $this->nombre = $nombre;
    }

    public function setEdad(int $edad): void {
        $this->edad = $edad;
    }

    public function setDni(?string $dni): void {
        $this->dni = $dni;
        
        $this->generaDNI();
    }

    public function setSexo(string $sexo): void {
        $this->sexo = $sexo;
        $this->comprobarSexo();
    }

    public function setPeso(int $peso): void {
        $this->peso = $peso;
    }

    public function setAltura(int $altura): void {
        $this->altura = $altura;
    }

    public function esMayorDeEdad () :bool{
        
        if ($this->edad>=18) {
            return true;
        } else {
            return false;
        }
    }
    public function __toString() {
        $resultado= $this->nombre."<br>";
        $resultado.= $this->edad."<br>"; //con .. es lo anterior y ademas esto....
        $resultado.= $this->altura."<br>";
        $resultado.= $this->peso."<br>";
        $resultado.= $this->sexo."<br>";
        $resultado.= $this->dni."<br>";
        
        return $resultado;
    }
    
    private function generaDNI():void{
        $letras =["T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E"];
        
        $posicion= $this->dni%23;
        
       $this->dni=$this->dni.$letras[$posicion]; 
    }
    
    private function comprobarSexo (){
        
        if (!(strtoupper($this->sexo=="H" || $this->sexo=="M" ))) { 
            $this->sexo="H";
            
            
        }
        
    }
}
