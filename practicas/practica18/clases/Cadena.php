<?php


/**
 * Description of clases
 *
 * @author Mario
 */
class Cadena {
    //put your code here
    
    private string $valor;
    private int $longitud;
    private int $vocales;
    
    public function __construct(string $valor) {
        $this->valor = $valor;
        
    }
    public function getValor (bool $minusculas=false){
        
                       
        if ($minusculas) {
            
            return strtolower($this->valor);
            
        } else {
            return $this->valor;
            
        }
        
        return $minusculas;
        
    }
    
     public function setValor($valor){
         $this->valor=$valor;
         
                 
    }
    
    public function getLongitud(){
                
        $this->calculaLongitud();
                
        return $this->longitud;
    
    }
    
      private function calculaLongitud():void{
        
          $this->longitud= strlen($this->valor);
          
    }
    
    
    public function getVocales(){
        
       $this->numeroVocales();
       return $this->vocales;
    }
    
    
    private function numeroVocales(){
        
        $vocales=['a','e','i','o','u'];
        $numero=0;
        
        foreach ($vocales as $vocal) {
           $numero+=substr_count($this->getValor(true),$vocal);
            
        }
        $this->vocales=$numero;
    
        
    }
    
      
    
    
   
   
    public function repeticionVocal(string $vocal):int{
        
        return substr_count($this->getValor(),$vocal);
        
    }

}
