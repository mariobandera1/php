<?php
$nombre="pepe";
$poblacion="porahi";


echo "<div>";
echo $nombre;
echo "</div>";
echo "<div>";
echo $poblacion;
echo "<div/>";

//solucion 2

echo "<div>".$nombre. "</div>";

echo "<div>".$poblacion . "</div>";

//solucion 3

echo "<div>$nombre</div>";
echo "<div>$poblacion</div>";

//utilizando heredoc

$salida=<<<"HEREDOC"
        <div>{$nombre}</div>
        <div>{$poblacion}</div>
        
        HEREDOC;
        
        echo $salida;