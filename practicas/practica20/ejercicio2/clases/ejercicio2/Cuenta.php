<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Cuenta
 *
 * @author Mario
 */

namespace clases\ejercicio2;

class Cuenta {

    private $numCuenta;
    private $saldo;

    public function __construct($numCuenta = 0, $saldo = 0) {
        $this->numCuenta = $numCuenta;
        $this->saldo = $saldo;
    }

    public function getNumCuenta() {
        return $this->numCuenta;
    }

    public function getSaldo() {
        return $this->saldo;
    }

    public function setNumCuenta($numCuenta): void {
        $this->numCuenta = $numCuenta;
    }

    public function setSaldo($saldo): void {
        $this->saldo = $saldo;
    }

    public function abonos(float $ingreso): void {

        $this->saldo += $ingreso;
    }

    public function abonoRecibo(float $pagos) {

        $this->saldo -= $pagos;
    }

//p}ut your code here
}
