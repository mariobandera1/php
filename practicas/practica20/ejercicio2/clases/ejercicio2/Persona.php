<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Persona
 *
 * @author Mario
 */

namespace clases\ejercicio2;

class Persona {

    private ?string $dni ;
    private ?string $nombre;
    private ?string $apellidos;
    private ?string $direccion;
    private array $telefono = [];
    public array $cuentas = [];

    public function __construct(?string $dni = null, ?string $nombre = null, ?string $apellidos = null, ?string $direccion = null, string $telefono =null , ?Cuenta $cuenta = null) {
        $this->dni = $dni;
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->direccion = $direccion;
        
        if (!is_null($telefono)){
            $this->telefonos[]=$telefono;//comprobamos si me han pasado un telefono
        }
        $this->telefonos[] = $telefono;
        
        if (!is_null($cuenta)) { // si no ponemos esto, inicializa la primera posicion del array
            $this->cuentas[]=$cuenta;
        }
        
        
    }

    public function getDni(): string {
        return $this->dni;
    }

    public function getNombre(): string {
        return $this->nombre;
    }

    public function getApellidos(): string {
        return $this->apellidos;
    }

    public function getDireccion(): string {
        return $this->direccion;
    }

    public function getTelefono(): array {
        return $this->telefono;
    }

    public function getCuenta(): array {
        return $this->cuentas;
    }

    public function setDni(string $dni): void {
        $this->dni = $dni;
    }

    public function setNombre(string $nombre): void {
        $this->nombre = $nombre;
    }

    public function setApellidos(string $apellidos): void {
        $this->apellidos = $apellidos;
    }

    public function setDireccion(string $direccion): void {
        $this->direccion = $direccion;
    }

    public function setTelefonos(array $telefono): void {
        $this->telefono = $telefono;
    }

    public function setCuenta(array $cuenta): void {
        $this->cuenta = $cuenta;
    }

    public function añadirCuenta(cuenta $cuenta): void {
        if (count($this->cuentas) < 3) {
            $this->cuentas[] = $cuenta;
        }
    }

    public function moroso(): bool {

        foreach ($this->cuentas as $cuenta) {

            if ($cuenta->getSaldo() < 0) {

                return true;
            }
        }
            return false; //si no devuelve nada da error, y espera un boolean, por eso debe estar como false
        
    }
    
    public function agregarTelefono(String $telefono):void{
        
        $this->telefono[]=$telefono;
        
    }

}
