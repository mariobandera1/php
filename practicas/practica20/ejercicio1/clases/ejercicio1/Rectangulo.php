<?php

namespace clases\ejercicio1;

class Rectangulo {

    private float $base = 0;
    private float $altura = 0;

    public function __construct(float $base, float $altura) {
        $this->base = $base;
        $this->altura = $altura;
    }

    public function getBase(): float {
        return $this->base;
    }

    public function getAltura(): float {
        return $this->altura;
    }

    public function setBase(float $base): void {
        $this->base = $base;
    }

    public function setAltura(float $altura): void {
        $this->altura = $altura;
    }

    public function area():float {

        $area = ((($this->altura) * ($this->base)) );

        return $area;
    }

    public function perimetro():float {
        
        $perimetro = ($this->base*2)+($this->altura*2);
        
        return $perimetro;
        
    }

//put your code here
}
