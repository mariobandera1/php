<?php

spl_autoload_register(function ($nombre_clase) { //aqui el autoload lo hemos hecho con namespace en Rectangulo
    include  $nombre_clase . '.php';
});

$rectangulo1=new clases\ejercicio1\Rectangulo(16.5, 20.4);

$rectangulo2=new clases\ejercicio1\Rectangulo(25.4, 35.4);


var_dump($rectangulo1);
echo "<br>";

echo "el area del rectangulo es ".$rectangulo1->area();
echo "<br>";
echo "el perimetro del rectangulo es ".$rectangulo1->perimetro();
echo "<br>";

echo "el area del rectangulo es ".$rectangulo2->area();
echo "<br>";
echo "el perimetro del rectangulo es ".$rectangulo2->perimetro();
echo "<br>";

var_dump($rectangulo1);


