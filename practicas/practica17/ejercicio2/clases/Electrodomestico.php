<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Electrodomestico
 *
 * @author Mario
 */
class Electrodomestico {
    
    public string $tipo;
    
    public string $marca;
    
    public float $potencia;
    
    public function __construct(string $tipo, string $marca, float $potencia) {
        $this->tipo = $tipo;
        $this->marca = $marca;
        $this->potencia = $potencia;
    }

    public  function getTipo(): string  {
        return $this->tipo;
    }

    public function getMarca(): string {
        return $this->marca;
    }

    public function getPotencia(): float {
        return $this->potencia;
    }

    public function setTipo(string $tipo): void {
        $this->tipo = $tipo;
    }

    public function setMarca(string $marca): void {
        $this->marca = $marca;
    }

    public function setPotencia(float $potencia): void {
        $this->potencia = $potencia;
    }
    
    public function __toString() {
        
        $resultado1=$this->tipo." , ".$this->marca." , ".$this->potencia." ,";
        
        return $resultado1;

    }
    
    public function getConsumo(int $horas){
        
        $consumo = $this->potencia*$horas;
        
        return $consumo;
        
    }
    
    public function getCosteConsumo(int $horas,float $costeHora){
        
        $costeConsumo=$this->potencia*$horas*$costeHora;
        
        return $costeConsumo;
    }
}
