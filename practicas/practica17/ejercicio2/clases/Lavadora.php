<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Lavadora
 *
 * @author Mario
 */


 class Lavadora extends Electrodomestico {
    //put your code here
    
    public float $precio;
    public bool  $aguaCaliente;
    
  public function __construct(float $precio, bool $aguaCaliente,string $marca,float $potencia){
     
         parent::__construct("Lavadora",$marca,$potencia);
        $this->precio = $precio;
        $this->aguaCaliente = $aguaCaliente;
    }
    public function getPrecio(): float {
        return $this->precio;
    }

    public function getAguaCaliente(): bool {
        return $this->aguaCaliente;
    }

    public function setPrecio(float $precio): void {
        $this->precio = $precio;
    }

    public function setAguaCaliente(bool $aguaCaliente): void {
        $this->aguaCaliente = $aguaCaliente;
    }
    public function getMarca(): string {
        return parent::getMarca();
    }

    public function getPotencia(): float {
        return parent::getPotencia();
    }

    public function getTipo(): string {
        return parent::getTipo();
    }

    public function setMarca(string $marca): void {
         parent::setMarca($marca);
    }

    public function setPotencia(float $potencia): void {
         parent::setPotencia($potencia);
    }

    public function setTipo(string $tipo): void {
         parent::setTipo($tipo);
    }
    
       public function __toString(): string {
        $salida= parent::__toString();
        $salida.="<br>Precio={$this->precio}<br>";
        $salida.="Agua Caliente={$this->aguaCaliente}<br>";
        return $salida;
        
    }
    
    public function getConsumo(int $horas) {
        if ($this->aguaCaliente) {
            
           return $horas*($this->potencia+$this->potencia*0.2);
            
        } else {
            return $this->potencia*$horas;            
        }
    }

    
      
    
    
    
 }  

